import { Routes } from "@angular/router";
import { MainComponent, NotFound } from "../Components/index";




export const NavigationRoutes: Routes = [
    { path: "", redirectTo: "/main", pathMatch: "full" },
    { path: "main", component: MainComponent },
    { path: "401", component: NotFound }
   

];


