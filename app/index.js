let Generator = require('yeoman-generator');
let mkdir = require('mkdirp');
let yosay = require('yosay');
let chalk = require('chalk');
let genConfig = require('./generatorConfig.js');
let _ = require("lodash");


let angular_version = "4.3.6";

module.exports = class extends Generator {


    contructor(args, opts) {
        // super(args, opts);
        // Generator.Base.apply(this, arguments);

        this.argument('appName', { required: true, type: String, description: "Application Name", default: this.appname });
        this.log(this.options.appName);
        // this.log(this.options);

    }

    initializing() {
        this.props = {};

        this.rootDir = "";
        this.appRoot = "";
        this.srcRoot = "";
        this.tmpl = this.sourceRoot();

    }

    prompting() {

        this._showWelcomeMessage();

    }

    configuring() { }

    default() { }

    writing() {

        this._createDirectoryStructure();
        this._copyRootDirectoryFiles();
        this._createToolsFiles();
        this._createAssetFiles();
        this._copyAppRootDirectoryAndFiles();

    }

    conflicts() { }

    install() {


        // this.spawnCommand("npm install -g yarn");

        this._installDependencies();

        this._runTscOnGulp();




        // this._runTscOnTypescriptDirectory();

    }

    end() { 

        
    }


    _showWelcomeMessage() {

        // let angular_version = "4.3.6";

        let message = chalk.green.bold("Sly Angular Project Starter")
            + chalk.white(`\n Template is build on Angular version: ${angular_version}`);

        this.log(yosay(message, { maxLength: 40 }));
    }

    _createDirectoryStructure() {
        // console.log(`destinationRoot: ${this.destinationRoot()}`);
        // console.log(`destinationPath: ${this.destinationPath()}`);

        // console.log(`AppName: ${this.appName}`);
        let applicationName = _.snakeCase(this.options.appName);
        let rootDir = `${this.destinationRoot()}\\${applicationName}`;

        this.rootDir = `${rootDir}\\`;
        this.appRoot = `${rootDir}\\app`;
        this.srcRoot = `${rootDir}\\src`;

        // console.log(`rootDir: ${rootDir}`);

        // Create an  App and Src Directory
        mkdir(this.rootDir);
        mkdir(this.appRoot);
        mkdir(this.srcRoot);



    }

    _createAssetsDirectoryStructure() {

        // Create Assets Directory
        let assetDir = `${this.srcRoot}\\assets`;
        mkdir(assetDir);


        // Make Assets Subdirectories 
        let subdirectories = ["imgs", "less", "ts"];

        subdirectories.forEach((directory) => {
            mkdir(`${assetDir}\\${directory}`);
        });

        // Copy Assets Files
        this._createAndCopyLessFiles();
        this._createAndCopyAngularTypescriptFiles();

    }

    _createToolsFiles() {

        let tools = `${this.srcRoot}\\${genConfig.TOOLS.TOOLS_DIRECTORY}\\`;
        // console.log(`Tools: ${tools}`);
        mkdir(tools);

        console.log(`templateRoot: ${this.tmpl}`);

        this.fs.copy(
            `${this.tmpl}\\tools\\**\\*.*`,
            tools
        );

    }

    _createAssetFiles() {

        this._createAssetsDirectoryStructure();

    }

    _createAndCopyLessFiles() {

        let lessDir = `${this.srcRoot}\\assets\\less`;

        this.fs.copy(
            `${this.tmpl}\\src\\assets\\less\\**\\*.*`,
            lessDir
        );
    }

    _createAndCopyAngularTypescriptFiles() {

        let TsDir = `${this.srcRoot}\\assets\\ts`;

        this.fs.copy(
            `${this.tmpl}\\src\\assets\\ts\\**\\*.*`,
            TsDir
        );

        // this._copyTsConfig(TsDir);

    }

    _copyTsConfig(TsDir) {
        this.fs.copy(
            `${this.tmpl}\\src\\assets\\ts\\tsconfig.json`,
            TsDir
        );
    }

    _copyRootDirectoryFiles() {

        this.fs.copy(
            `${this.tmpl}\\*.*`,
            this.rootDir
        );
    }

    _copyAppRootDirectoryAndFiles() {

        // Copy Index HTML File
        this._copyIndexHtmlFile();

        // Copy App Root Assets
        this._copyAppAssets();

    }


    _copyIndexHtmlFile() {

        this.fs.copyTpl(
            `${this.tmpl}\\app\\index.html`,
            `${this.appRoot}\\index.html`,
            {
                appName: this.options.appName
            }
        );
    }

    _copyAppAssets() {

        this.fs.copy(
            `${this.tmpl}\\app\\assets\\**\\*.*`,
            `${this.appRoot}\\assets\\`
        );
    }

    _runTscOnGulp() {

        console.log('Starting Promise');
        var promise = new Promise((resolve, reject) => {

            process.chdir(`${this.srcRoot}`);

            this.spawnCommandSync("tsc");

            resolve();

        });

        return promise;
        // process.chdir(`${this.srcRoot}`);

        // this.spawnCommandSync("tsc");

        // promise.then(() => {
        //     console.log('Promise Resolved');
        //     this._runTscOnTypescriptDirectory();
        // });


    }

    _runTscOnTypescriptDirectory() {

        process.chdir(`${this.srcRoot}\\assets\\ts`);

        this.spawnCommandSync("tsc");

    }

    _installDependencies() {
        var promise = new Promise((resolve, reject) => {

            process.chdir(this.rootDir);

            this.installDependencies({
                npm: true,
                bower: false,
                yarn: false
            });
            resolve();
        });

        return promise;
    }


};

