import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";


const <%= moduleNameLc %>Routes: Routes = [

    {
        path: "",
        children: [
            { path: "", component: ComponentName }
        ]
    }


];

export const <%= moduleName %>ProviderRoutes: ModuleWithProviders = RouterModule.forChild(<%= moduleNameLc %>Routes);

