import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule } from "@angular/forms";
import { <%= moduleName %>ProviderRoutes } from "./Routing/index";



@NgModule({
    imports:    [
                    BrowserAnimationsModule,
                    FormsModule,
                    CommonModule,
                    <%= moduleName %>ProviderRoutes
                ],
    exports:    [

                ],
    declarations:
                [

                ],
    providers:  [

                ]
})


export class <%= moduleName %>Module {

    public static forRoot(): ModuleWithProviders {

    return {
      ngModule: <%= moduleName %>Module,
      providers: [  ]
    };
  }
 }
