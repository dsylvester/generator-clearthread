let Generator = require('yeoman-generator');
let mkdir = require('mkdirp');
let _ = require("lodash");


module.exports = class extends Generator {


    contructor(args, opts) {


        this.argument('moduleName', { required: true, type: String, description: "Module Name" });
        // this.log(this.options.moduleName);
        // this.log(this.options);



    }

    initializing() {

        this.moduleRootDir = "";
        this.ModuleName = "";
        this.tmpl = this.sourceRoot();



    }

    prompting() {


    }

    configuring() {


    }

    default() {

        let tempModuleName = this.options.moduleName;
        console.log(`tempModuleName: ${tempModuleName}`);


        let moduleName = _.snakeCase(tempModuleName);
        this.ModuleName = this._pascalCaseNoSpaces(moduleName);

        let moduleRootDir = `${this.destinationRoot()}\\${this._CamelCaseWithoutUnderscore(this.ModuleName)}`;
        this.moduleRootDir = moduleRootDir;

        
        // Make sure they are at the Typescript Root Directory
        // console.log(`destinationRoot: ${this.destinationRoot()}`);
        
        // destRoot:   C:\projects\Davaco\Angular-Apps\VTours\src\assets\ts

    

        
        if (this._usingOrInTSDirectory()) {
            mkdir(this.moduleRootDir);
        }
        else {
            throw new Error("Can't Find TS Directory!  You must be in the TS Directory to create a new Module");
        }
        

    }


    writing() {

        this._createModuleDirectoryStructure();
        this._copyAllFiles();

    }


    conflicts() { }

    install() { }

    end() { }


    _createModuleDirectoryStructure() {

        let folders = [
            "Classes",
            "Interfaces",
            "Components",
            "Html",
            "Routing",
            "Models",
            "Services",
            "Helpers"
        ]

        folders.forEach((folder) => {

            mkdir(`${this.moduleRootDir}\\${folder}`);
        });


    }

    _copyAllFiles() {

        this._copyModuleTypescriptFile();
        this._copyModuleTypescriptIndexFile();
        this._copyRoutingFolder();



    }
    _copyModuleTypescriptFile() {

        this.fs.copyTpl(
            `${this.tmpl}\\moduleName.module.ts`,
            `${this.moduleRootDir}\\${this._CamelCaseWithoutUnderscore(this.ModuleName)}.module.ts`,
            {
                moduleName: this._CamelCaseWithoutUnderscore(_.capitalize(this.ModuleName))
            }
        );
    }

    _CamelCaseWithoutUnderscore(val) {

        let lc = _.lowerCase(val);

        let convert = (x) => {
            let aa = x.split(" ");
            
            let cc = "";
            
            aa.forEach((b) => {
                cc = `${cc}${_.upperFirst(b)}`;
            });

            return cc;
          };

        return convert(lc);

    }

    _CamelCaseWithUnderscore(val) {

        let mName = _.snakeCase(val);

        let lc = _.lowerCase(mName);

        let name = lc.replace(/\s/g, "_");

        let convert = (val) => {
            let aa = val.split("_");
    
            let cc = "";
    
            aa.forEach((b) => {
    
                cc = `${(cc !== "") ? `${cc}_` : ""}${_.upperFirst(b)}`;
            });
    
            return cc;
        };

        return convert(name);
    }


    _copyModuleTypescriptIndexFile() {

        this.fs.copyTpl(
            `${this.tmpl}\\index.ts`,
            `${this.moduleRootDir}\\index.ts`,
            {
                moduleName: this._CamelCaseWithoutUnderscore(this.ModuleName)
            }
        );
    }

    _copyRoutingFolder() {

        this._copyRoutingFile();
        this._copyRoutingIndexFile();

    }

    _copyRoutingFile() {

        this.fs.copyTpl(
            `${this.tmpl}\\module.routing.ts`,
            `${this.moduleRootDir}\\Routing\\${this._CamelCaseWithoutUnderscore(this.ModuleName)}.routing.ts`,
            {
                moduleName: this._CamelCaseWithoutUnderscore(this.ModuleName),
                moduleNameLc: this._CamelCaseWithoutUnderscore(this.ModuleName)
            }
        );

    }

    _copyRoutingIndexFile() {

        this.fs.copyTpl(
            `${this.tmpl}\\index.routing.ts`,
            `${this.moduleRootDir}\\Routing\\index.ts`,
            {
                moduleName: this._CamelCaseWithoutUnderscore(this.ModuleName),
                moduleNameLc: this._CamelCaseWithoutUnderscore(this.ModuleName)
            }
        );

    }

    _usingOrInTSDirectory() {

        // DestRoot = C:\temp\testing\test_123\src\assets\ts\Location_profile
        let destRoot = this.destinationRoot();

        let valid = true;

        if (destRoot.toLowerCase().indexOf("ts") !== -1 &&
            this._lastElementIsEqual(destRoot, "ts")) {
            return valid;
        }

        valid = false;

        return valid;
    }

    _lastElementIsEqual(path, searchName) {

        let seperator = "";

        if (path.indexOf("/") !== -1) {
            seperator = "/";
        }
        else {
            seperator = "\\";
        }

        let temp = path.split(seperator);

        // Chceck to see if TS is there
        console.log(`Does TS Exist: ${temp.indexOf("ts")}`);

        return temp[temp.length - 1] === searchName;

    }

    _tryChangeToComponentDirectory() {

        try {
            process.chdir("TS");
            
        }

        catch (e) {
            throw new Error("You must be in the TS directory to create a new Module");

        }
    }

    _pascalCaseNoSpaces(val) {
        // console.log(`Val: ${val}`);
        // console.log(`val.indexOf ${val.indexOf(" ")}`);

        if (val.indexOf(" ") !== -1) {
            let temp = String(val).split(" ");

            // console.log(temp);
            let result = "";

            var tempResult = temp.forEach((x) => {
                result = result + `${_.capitalize(x)}`;
                // console.log(`Result: ${result}`);
            });

            return result.substring(0, result.length);
        } else {
            return _.capitalize(val);
        }
    
    }

};

