let Generator = require('yeoman-generator');
let mkdir = require('mkdirp');
let _ = require("lodash");


module.exports = class extends Generator {


    contructor(args, opts) {


        this.argument('serviceName', { required: true, type: String, description: "Enter your Helper Name without Helper ending" });       
        

    }

    initializing() {

        this.moduleRootDir = "";
        this.ServiceName = "";
        this.ServiceRootDir = "";
        this.HtmlRootDir = "";
        this.tmpl = this.sourceRoot();



    }

    prompting() {


    }

    configuring() {


    }

    default() {

        let tempServiceName = this.options.serviceName;

        let ServiceName = _.snakeCase(tempServiceName);
        this.ServiceName = _.capitalize(ServiceName);
        this.ServiceNameLc =  _.chain(this.ServiceName).lowerCase();
        this.ServiceNameCamelSnakeCase =  this._checkForSpace(_.chain(this.ServiceName).lowerCase());
        this.ServiceNameNoSpaces = this._pascalCaseNoSpaces(_.chain(this.ServiceName).lowerCase());

        let ServiceRootDir = "";
       
        let destRoot = this.destinationRoot();
        // console.log(`destinationRoot: ${this.destinationRoot()}`);

        let needToChangeToServiceDirectory = false;


        if (!this._usingOrInServiceDirectory()) {
             
            this._tryChangeToServiceDirectory();
            ServiceRootDir = `${this.destinationRoot()}\\Helpers`;
            // console.log(`Current Path: ${this.destinationRoot()}`);
            // console.log(`Service Dircetory: ${ServiceRootDir}`);
            
            this.ServiceRootDir = ServiceRootDir;           
            this.HtmlRootDir = `${this.destinationRoot()}\\Html`;
            
        }
        
        



    }


    writing() {

        this._createNewService();
    }


    conflicts() { }

    install() { }

    end() { }

    _createNewService() {

        this._copyIndexTypescriptFile();
        this._copyServiceFile();
        // this._copyServiceHtmlFile();
        // this._copyServiceLessFile();
    }

    
    _usingOrInServiceDirectory() {

        // DestRoot = C:\temp\testing\test_123\src\assets\ts\Location_profile
        let destRoot = this.destinationRoot();

        let valid = true;

        if (destRoot.toLowerCase().indexOf("Helpers") !== -1 &&
            this._lastElementIsEqual(destRoot.toLowerCase(), "Helpers")) {
            return valid;
        }

        valid = false;

        return valid; 
    }

    _lastElementIsEqual(path, searchName) {
        
        let seperator = "";

        if (path.indexOf("/") !== -1) {
            seperator = "/";
        }
        else {
            seperator = "\\";
        }

        let temp = path.split(seperator);  

        return temp[temp.length - 1] === searchName;

    }

    _tryChangeToServiceDirectory() {

        try {
            process.chdir("Helpers");
            console.log("I changed to Helper Directory");
        }

        catch(e) {
            throw new Error("You must be in the Helpers directory to create a new Helper");

        }
    }

    _indexTypescriptFileExists() {
       return this.fs.exists(`${this.ServiceRootDir}\\index.ts`);
    }

    _copyIndexTypescriptFile() {

        // console.log(`ServiceRootDir: ${this.ServiceRootDir}`);
        // console.log(`this.ServiceNameNoSpaces: ${this.ServiceNameNoSpaces}`);
        // console.log(`this.ServiceNameCamelSnakeCase: ${this.ServiceNameCamelSnakeCase}`);
        // console.log(`this.ServiceNameLc: ${this.ServiceNameLc}`);


        if (!this._indexTypescriptFileExists()) {
            this.fs.copyTpl(
                `${this.tmpl}\\index.helper.ts`,
                `${this.ServiceRootDir}\\index.ts`,
                {
                    ServiceName: this.ServiceNameNoSpaces,
                    ServiceNameCamelSnakeCase: this.ServiceNameNoSpaces
                }
            );
        }
    }

    _copyServiceFile() {
        this.fs.copyTpl(
            `${this.tmpl}\\helper.ts`,
            `${this.ServiceRootDir}\\${this.ServiceNameNoSpaces}Helper.ts`,
            {
                ServiceName: this.ServiceNameNoSpaces,
                ServiceNameCamelSnakeCase: this.ServiceNameNoSpaces
            }
        );
    }

    
    _checkForSpace(val) {
        // console.log(`Val: ${val}`);
        // console.log(`val.indexOf ${val.indexOf(" ")}`);

        if (val.indexOf(" ") !== -1) {
            let temp = String(val).split(" ");

            // console.log(temp);
            let result = "";

            var tempResult = temp.forEach((x) => {
                result = result + `${_.capitalize(x)}_`;
                // console.log(`Result: ${result}`);
            });

            return result.substring(0, result.length - 1);
        }
    }

    _pascalCaseNoSpaces(val) {
        // console.log(`Val: ${val}`);
        // console.log(`val.indexOf ${val.indexOf(" ")}`);

        if (val.indexOf(" ") !== -1) {
            let temp = String(val).split(" ");

            // console.log(temp);
            let result = "";

            var tempResult = temp.forEach((x) => {
                result = result + `${_.capitalize(x)}`;
                // console.log(`Result: ${result}`);
            });

            return result.substring(0, result.length);
        }
    }

};

