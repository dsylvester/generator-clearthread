import { Injectable } from "@angular/core";


@Injectable()
export class <%= ServiceNameCamelSnakeCase %>Helper {

    constructor() {

    }

    private hasData(data: any): boolean {

        if (data !== undefined || data !== null) {
            return true;
        }
        else {
            return false;
        }
    }
}
